import React, { Component } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Login from './components/views/login';
import Inicio from './components/views/inicio'
import Lista from './components/views/list'
import User from './components/views/user'
import Manga from './components/views/manga'

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function TabMenu({navigation}) {
  return (
    <NavigationContainer independent={true}>
      <Tab.Navigator
        initialRouteName="Inicio"
        activeColor="white"
        color={"gray"}
        barStyle={{backgroundColor: '#5D5D5D'}}
      >
        <Tab.Screen 
          name="Inicio"
          component={Inicio}
          options={{tabBarLabel: 'Inicio', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="ballot" color={color} size={25} />)}}
        />        
        <Tab.Screen
          name="Lista"
          component={Lista}
          options={{tabBarLabel: 'Mangas', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="alpha-m-box" color={color} size={25} />)}}
        />
        <Tab.Screen
          name="Manga"
          component={Manga}
          tabBarIcon
          options={{tabBarLabel: 'Details', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="account" color={color} size={25} />)}}
        />
        <Tab.Screen
          name="User"
          component={User}
          tabBarIcon
          options={{tabBarLabel: 'Yo', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="account" color={color} size={25} />)}}
        />
        
        
        
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="TabMenu"
            component={TabMenu}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Manga"
            component={Manga}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
