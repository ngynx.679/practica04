import React, { Component } from 'react';

import {StyleSheet, View, BackHandler, Linking} from 'react-native';

import {Text, Avatar, Title, List, Button as ButtonPaper} from 'react-native-paper';

import {ScrollView} from 'react-native-gesture-handler';

function User({navigation}) {
        return (
        <ScrollView>
        <View>
            <View style={{alignItems: 'center', padding: 10}}>
            <Avatar.Image
                size={150}
                alignItems={'center'}
                source={require('../img/login.png')}
            />
            <Title>USUARIO</Title>
            </View>
            <View>
            <List.Section style={{padding: 10}}>
                <List.Subheader>Ajustes de la Aplicacion</List.Subheader>
                <List.Item
                left={() => (
                    <ButtonPaper
                    icon={'book-open-page-variant'}
                    mode={'text'}
                    onPress={() => {
                        Linking.openURL('https://google.com');
                    }}>
                    <Text style={styles.textbutton}>Politica de Privacidad</Text>
                    </ButtonPaper>
                )}
                />
                <List.Item
                left={() => (
                    <ButtonPaper
                    icon={'book-open-page-variant'}
                    mode={'text'}
                    onPress={() => {
                        Linking.openURL('https://google.com');
                    }}>
                    <Text style={styles.textbutton}>Terminos y Condiciones</Text>
                    </ButtonPaper>
                )}
                />
                <List.Item
                left={() => (
                    <ButtonPaper
                    icon={'book-open-page-variant'}
                    borderColor={'#000000'}
                    mode={'text'}
                    onPress={() => {
                        Linking.openURL('https://google.com');
                    }}>
                    <Text style={styles.textbutton}>Normas de la Comunidad</Text>
                    </ButtonPaper>
                )}
                />
                <List.Item
                left={() => (
                    <ButtonPaper
                    icon={'information'}
                    borderColor={'#896100'}
                    mode={'text'}
                    onPress={() => {
                        Linking.openURL('https://google.com');
                    }}>
                    <Text style={styles.textbutton}>Sobre la App</Text>
                    </ButtonPaper>
                )}
                />
                <List.Item
                left={() => (
                    <ButtonPaper
                    icon={'google-maps'}
                    borderColor={'#896100'}
                    mode={'text'}
                    onPress={() => BackHandler.exitApp()}>
                    <Text style={styles.textbutton}>Ubicanos</Text>
                    </ButtonPaper>
                )}
                />
                <List.Item
                left={() => (
                    <ButtonPaper
                    icon={'exit-to-app'}
                    borderColor={'#896100'}
                    mode={'text'}
                    onPress={() => BackHandler.exitApp()}>
                    <Text style={styles.textbutton}>Salir</Text>
                    </ButtonPaper>
                )}
                />
            </List.Section>
            </View>
        </View>
    </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      paddingHorizontal: 10,
      alignItems: 'center',
      color: '#000000',
      backgroundColor: '#FFAB38',
    },
    text: {
      alignItems: 'center',
      padding: 10,
    },
    textbutton: {
      color: '#000000',
    },
    image: {
      width: 200,
      height: 200,
    },
    button: {
      top: 10,
      alignItems: 'center',
      backgroundColor: '#271F0F',
      padding: 5,
    },
    TextInput: {
      borderColor: 'transparent',
      height: 40,
      width: 300,
      borderWidth: 1,
      textAlign: 'center',
    },
    Menu: {
      flex: 1,
      backgroundColor: '#D8D8D8',
      justifyContent: 'space-between',
      padding: 5,
    },
  });

export default User;