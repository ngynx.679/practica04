import React, { Component } from 'react';
import {StyleSheet, View, TouchableOpacity, SafeAreaView, FlatList, Image} from 'react-native';
import {Text, Searchbar, Card, Title, Paragraph, Button, Avatar, ActivityIndicator, Badge} from 'react-native-paper';
import {ScrollView} from 'react-native-gesture-handler';
import firebase from '../../firebase';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const generos = [
  {id: 1, title: "Shounen", image: require('../img/shounen.png')},
  {id: 2, title: "Shoujo", image: require('../img/shoujo.png')},
  {id: 3, title: "Seinen", image: require('../img/seinen.png')},
  {id: 4, title: "Kodomo", image: require('../img/kodom.png')},
  {id: 5, title: "Mecha", image: require('../img/mecha.png')},
  {id: 6, title: "Gore", image: require('../img/gore.png')},
]


class Inicio extends Component {
  
  constructor() {
    super();
    this.state = {
      isLoading: true,
      mangaArr: []
    }
  }
  async componentDidMount() {
    let data = []
    await firebase.database().ref('/mangas')
    .once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childData = childSnapshot.val();
        data.push(childData)
      });
    });
    this.setState({ mangaArr: data });
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Tipos de Manga</Text>
        <SafeAreaView>
          <FlatList style={{padding: 5}}
            data={generos}
            horizontal= {true}
            showsHorizontalScrollIndicator={false}
            legacyImplementation={false}
            numColumns={1}
            keyExtractor= {(item) => {
              return item.id;
            }}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity style={styles.iconCard} >
                  <Avatar.Image style={{backgroundColor: 'transparent'}} size={75} source={item.image} />
                  <View>
                    <View>
                      <Text style={{fontSize: 10, color: '#800000' }}>{item.title}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            }}/>
          </SafeAreaView>
          <Text style={styles.text}>Mangas mejor Valorados</Text>
          <ScrollView>
          <FlatList style={styles.list}
            contentContainerStyle={styles.listContainer}
            data={this.state.mangaArr.length > 0 ? this.state.mangaArr : []}
            horizontal={false}
            numColumns={2}
            ListEmptyComponent={() => (<ActivityIndicator style={{marginTop: 25}} animating={true} color='#800000' />)}
            keyExtractor= {(item) => {
              return item.id;
            }}
          renderItem={({item}) => {
            return (
              <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('Manga', {
                name:item.name, 
                img:item.img, 
                auth:item.auth, 
                description: item.description,
                img2: item.img2,
                intro:item.intro
                })}>
                <Badge size={24}>{item.valoracion}</Badge>
                <Avatar.Image size={100} source={{uri: item.img}} style={styles.cardImage} />
                <View style={styles.cardHeader}>
                  <View style={{alignItems:"center", justifyContent:"center"}}>
                    <Text style={styles.title}>{item.name}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )
          }}
        />
        </ScrollView>
        </View>
      

  );
  }
}


const styles = StyleSheet.create({
  container:{
    flex:1, backgroundColor: '#767676'
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 20,
    color:"#FFFFFF"
  },
  list: {
    paddingHorizontal: 5
  },
  listContainer:{
    alignItems:'center'
  },
  /******** card **************/
  iconCard:{
    backgroundColor:"#ffe4b5",
    alignItems:"center",
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    marginVertical: 2,
    shadowRadius: 7.49,
    elevation: 12,
    marginHorizontal: 5,
    justifyContent:"center",
    height: 100, width: 100
  },
  card:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    justifyContent: 'space-between',
    elevation: 12,
    marginVertical: 10,
    backgroundColor:"#ffe1ad",
    flexBasis: '42%',
    marginHorizontal: 10,
  },
  cardHeader: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems:"center", 
    justifyContent:"center"
  },
  cardContent: {
    paddingHorizontal: 16,
    paddingBottom: 8
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 8,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage:{
    alignSelf:'center'
  },
  title:{
    fontSize:20,
    fontWeight: 'bold',
    flex:1,
    alignSelf:'center',
    color:"#800000"
  },
})

export default Inicio;


