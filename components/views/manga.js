import React, { Component, useState, useRef } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//
import YoutubePlayer from 'react-native-youtube-iframe';

export default function Manga({route, navigation}) {
    const {name} = route.params;
    const {img} = route.params;
    const {auth} = route.params;
    const {description} = route.params;
    const {intro} = route.params;
    const playerRef = useRef(null);
    const [playing, setPlaying] = useState(true);

    return (
        <ScrollView>
            <View style={styles.container}>
                <ScrollView>
                    <View>
                        <Text style={styles.text}>Detalles</Text>
                        <Image source={{ uri: img }} style={styles.itemImage} resizeMode="cover" />
                    </View>
                        <Text style={styles.title}>{name}</Text>
                        <Text style={styles.auth}>Autor:  {auth}</Text>
                    <View>
                        <Text style={styles.txt1}>Resumen:</Text>
                        <Text style={styles.description}>{description}</Text>
                        <YoutubePlayer
                            ref={playerRef}
                            height={300}
                            width={400}
                            videoId={intro}
                            play={playing}
                            onChangeState={event => console.log(event)}
                            onReady={() => console.log("ready")}
                            onError={e => console.log(e)}
                            onPlaybackQualityChange={q => console.log(q)}
                            volume={50}
                            playbackRate={1}
                            playerParams={{
                                cc_lang_pref: "us",
                                showClosedCaptions: true
                            }}
                            />
                    </View>
                    <View>
                    </View>
                    <View>
                        <Text></Text>
                    </View>
                </ScrollView>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#767676',
    },
    text: {
        fontSize: 24,
        fontWeight: 'bold',
        paddingHorizontal: 20,
        color:"#FFFFFF",
        paddingBottom: 10
      },
    itemContainer: {
        backgroundColor: '#C0C841',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    description: {
        fontSize: 20,
        paddingLeft: 20,
        textAlign: 'justify',
        paddingRight: 20,
        color: '#FFFFFF'
    },
    itemImage: {
        height: 405,
        width: 360,
        alignSelf: 'center',
    },
    txt1: {
        fontSize: 25,
        paddingLeft: 10,
        fontFamily: 'sans-serif-condensed',
        textDecorationLine: 'underline',
        paddingBottom: 5,
        color: '#FFFFFf'

    },
    title: {
        paddingLeft: 10,
        fontSize: 40,
        fontFamily: 'sans-serif-condensed',
        color: '#FFFFFf',
        textAlign: 'center',
        paddingTop: 5,
        paddingBottom: 5

    },
    auth: {
        paddingLeft: 10,
        fontSize: 25,
        fontFamily: 'sans-serif-condensed',
        color: '#FFFFFf',
        paddingTop: 5,
        paddingBottom: 8
    },


});