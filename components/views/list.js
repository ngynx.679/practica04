import React, { Component } from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Text, List, Avatar} from 'react-native-paper';
import {ScrollView, FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import firebase from '../../firebase';



class Lista extends Component {
  
  constructor() {
    super();
    this.state = {
      isLoading: true,
      mangaArr: []
    }
  }
  async componentDidMount() {
    let data = []
    await firebase.database().ref('/mangas')
    .once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childData = childSnapshot.val();
        data.push(childData)
      });
    });
    this.setState({ mangaArr: data });
  }

  render() {
    const {navigation} = this.props;
    return (
      <View>
        <Text style={styles.head}>Lista de Mangas</Text>
        <Text style={styles.space}></Text>
          <ScrollView style={{backgroundColor: '#767676'}}>             
            <FlatList 
              data= {this.state.mangaArr.length > 0 ?
              this.state.mangaArr : []}
              keyExtractor = {(item) => {
                return item.id
              }}
              renderItem={({item}) => {
                return (
                  <TouchableOpacity>
                      <View style= {styles.container}>
                        <View style={{flex:1, flexDirection: 'row'}}>
                          <Image style={styles.imageView} source={{uri: item.img}} />
                            <View style= {styles.textView}>
                              <Text style= {styles.title}>{item.name}</Text>
                            </View>
                        </View>
                      </View>
                </TouchableOpacity> 
                )
              }}
            
            
            />
          </ScrollView>
      </View>
  );
  }
}

const styles = StyleSheet.create({
  loading: {
      flex: 1,
      padding:50,
      justifyContent: 'center',
      alignContent: 'center',
    },
  head: {
      fontSize: 25,
      textAlign: 'center',
      backgroundColor: '#767676',
      color: 'white',
  },
  container: {
    flex: 1,
    marginTop: 15,
    margin: 3,
    backgroundColor: '#767676',
  },
  imageView: {

      width: '30%',
      height: 90 ,
      margin: 20,
      borderRadius : 30, 
  },
  textView: {
      width:'45%', 
      textAlignVertical:'center',
      textAlign:'justify',
      justifyContent: 'center',
      fontFamily: 'Pacifico-Regular',
  },
  textvolver:{
      width: '25%',
      justifyContent: 'center'
      
  },
  item: {
    backgroundColor: 'gray',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    fontSize: 20,
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: 'Bangers',
  },
  space: {
    backgroundColor: '#767676',
    fontSize: 2,
  },
  titleloading: {
      fontSize: 40,
      color: '#c43c00',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
});
export default Lista;
