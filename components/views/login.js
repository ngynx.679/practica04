import React, { Component } from 'react';

import {Button, TextInput} from 'react-native-paper';

import { View, StyleSheet, Image, Alert } from 'react-native';

import { Text, Input} from 'react-native-elements';

import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            textValue: '',
            textPass: '',
            error: null,
        };
    }

    changeTextUser = (text) => {
        this.setState({textValue: text});
    }

    changeTextPass = (text) => {
        this.setState({textPass: text});
    }
    showAlert = () => {
        Alert.alert(
          'Acceso Denegado',
          'El usuario o la constraseña es incorrecta',
          [
            {
              text: 'OK',
              onPress: () => console.log('OK Pressed')
            },
          ],
          {
            cancelable: false
          },
        );
      };

    render() {
        const {navigation} = this.props;

        return (
            <View style={styles.container}>
                <Text h2>Iniciar Sesion</Text>
                <Image source={require('../img/login.png')}
                    style={styles.image} />
                <Input
                    placeholder='Usuario'
                    leftIcon={{ type: 'font-awesome', name: 'user', size: 40 }}
                    onChangeText={text => 
                        this.changeTextUser(text)}
                    value={this.state.textValue}
                />
                <Input
                    placeholder='Contraseña'
                    leftIcon={{ type: 'font-awesome', name: 'lock', size: 40 }}
                    secureTextEntry={true}
                    onChangeText={text => 
                        this.changeTextPass(text)}
                    value={this.state.textPass}
                />
                <Button
                    icon= "arrow-right-bold-box-outline"
                    mode="contained"
                    onPress={() => {
                        if (this.state.textValue == "admin" && this.state.textPass == "123") {
                            navigation.navigate('TabMenu')
                        }
                        else {
                            this.showAlert()
                        }
                    } }
                >
                    Ingresar
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
    },
    text: {
        alignItems:'center',
        padding: 10,
    },
    image: {
        width: 200,
        height: 200,
        
    }
});

export default Login;